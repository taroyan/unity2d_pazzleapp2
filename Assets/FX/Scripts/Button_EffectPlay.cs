﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement; 

public class Button_EffectPlay : MonoBehaviour {

	private string nowScene;


	void OnGUI () {
 		if(GUI.Button(new Rect(20,40,80,20), " Play")) {
			nowScene = SceneManager.GetActiveScene ().name;
			SceneManager.LoadScene(nowScene);
		}
	}
}
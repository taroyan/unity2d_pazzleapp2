﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyChara : Photon.MonoBehaviour
{

	public Animator animator_Enemy;
	// テキストスライドインアニメーター（敵キャラ）
	public Sprite	enemyNormal;
	// 敵キャラ通常
	public Sprite	enemyDamage;
	// 敵キャラダメージ
	public Sprite	enemyAttack;
	// 敵キャラ攻撃
	public AudioClip[]	audioClip;
	// オーディオクリップ配列
	public GameObject meChara;
	// 主人公オブジェクト
	public Sprite	heroinDamage;
	// 主人公キャラダメージ
	public Sprite	heroinNormal;
	// 主人公キャラ通常
	public GameObject	hitText_Enemy;
	// 敵のヒットテキストオブジェクト（canvas内にある）
	public GameSystem gSys;
	// メインのゲームシステム(スクリプトのクラス）

	// Use this for initialization
	void Start ()
	{
	

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void showEndEnemyVib ()
	{
		//テキストにEndを入れる
		//textObj.GetComponent<Text> ().text = "End";
		animator_Enemy.SetBool ("IsEnemyHit", false);	// 一旦falseにしてtrueで遷移（いいやり方別にあるはずだが）

		// スプライトを元に戻す
		// 敵キャラ
		SpriteRenderer nowRenderer = this.GetComponent<SpriteRenderer> ();	// 敵キャラのスプライトレンダラー
		nowRenderer.sprite = enemyNormal;									// 通常へ

		// 主人公キャラ
		SpriteRenderer heroinRenderer = meChara.GetComponent<SpriteRenderer> ();	// 主人公キャラのスプライトレンダラー
		heroinRenderer.sprite = heroinDamage;										// ダメージ顔へ

		// ヒットテキストを非表示にする（アニメーション終了時の関数＝この関数で非表示）
		hitText_Enemy.SetActive (false);

	}

	public void proc_ChangeEnemySpriteToDamege ()
	{
		int hitCnt = gSys.hitCntOneAtk;
		int hitDamage = gSys.hitCntOneAtk * gSys.MoveTimesPanel_Hoji * 100;	// ダメージ　Hit回数　* パネル移動回数 * 100 

		// 敵キャラ
		SpriteRenderer nowRenderer = this.GetComponent<SpriteRenderer> ();	// 敵キャラのスプライトレンダラー
		nowRenderer.sprite = enemyDamage;									// ダメージ顔へ

		// 主人公キャラ
		SpriteRenderer heroinRenderer = meChara.GetComponent<SpriteRenderer> ();	// 主人公キャラのスプライトレンダラー
		heroinRenderer.sprite = heroinNormal;										// 通常顔へ

		// サウンド再生処理
		proc_PlaySound ();

		// ヒットテキストを表示させる（アニメーション終了時の関数で非表示）
		hitText_Enemy.GetComponent<Text> ().text = hitCnt.ToString () + " HIT!! " + hitDamage.ToString ();	// 文字　例「2 HIT!! 200」
		hitText_Enemy.SetActive (true);
	}


	// サウンド再生処理
	private void proc_PlaySound ()
	{
		AudioSource audioSrc = this.GetComponent<AudioSource> ();
		int inx = Random.Range (0, 4);					// これで０から３となるらしい。（４は含まないらしい）

		audioSrc.clip = audioClip [inx];				// ランダムな音声

		audioSrc.Play ();
	}
		
}

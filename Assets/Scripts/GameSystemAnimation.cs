﻿using UnityEngine;
using System.Collections;

public class GameSystemAnimation : Photon.MonoBehaviour
{
	
	public GameSystem GSys;
	// GameSystemのオブジェクト
	public Animator animator;
	// テキストスライドインアニメーター(タイトル）
	public Animator animator_Left;
	// テキストスライドインアニメーター（左リセット）
	public Animator animator_Right;
	// テキストスライドインアニメーター（右終了）
	public Animator animator_Enemy;
	// テキストスライドインアニメーター（敵キャラ）
	public Animator animator_MeChara;
	// テキストスライドインアニメーター（自分キャラ）



	private bool UIDispFlg;
	// UI表示フラグ

	// Use this for initialization
	void Start ()
	{
		UIDispFlg = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// UI表示系
		if ((GSys.utageDisplayFlg == false) && (UIDispFlg == false)) { 
			animator.SetBool ("IsCenter", true);
			animator_Left.SetBool ("IsLeft", true);
			animator_Right.SetBool ("IsRight", true);

			UIDispFlg = true;
		}

		// 敵キャラ振動
		if (GSys.EnemyHitFlg == true) {
			GSys.EnemyHitFlg = false;		// ここで落とす
			animator_Enemy.SetBool ("IsEnemyHit", true);
		}

		// 自分キャラ振動
		if (GSys.MeCharaHitFlg == true) {
			GSys.MeCharaHitFlg = false;		// ここで落とす
			animator_MeChara.SetBool ("IsMeCharaHit", true);
		}

	}
}

﻿using UnityEngine;
using System.Collections;

public class volTileSet : Photon.MonoBehaviour
{

	private const int maxTileNum = 9;
	// 最大値9枚
	private const int rowNum = 3;
	// 行数3

	public Sprite[] tiles;
	// タイル配列
	public GameSystem gameSysfromVol;
	// GameSystemのオブジェクト
	public AudioClip audioClip1;
	// 阿澄
	public AudioClip audioClip2;
	// 加藤
	public AudioClip audioClip3;
	// 原田
	public AudioSource audioSource;
	// オーディオソース

	private float duration;
	// サウンド再生時間
	private float elasptime;
	// サウンド経過時間
	private bool isDoubleTapStart;
	// ダブルタップ用
	private float doubleTapTime;
	// ダブルタップ用


	// Use this for initialization
	void Start ()
	{
		string test;
		test = this.name;
		
		gameSysfromVol = GameObject.Find ("GameSystem").GetComponent<GameSystem> ();	// GameSystemオブジェクトをパブリック変数へセットする
		// Photonの１台目も２台目も両方一度呼ばれるため、ここでrandomList_VolをPhotonの共通変数から再セットする。

		// Photonのパネル初期化フラグがfalseならば、端末の１台目でまだランダムの並びが生成されていないと判断
		// 逆に２台目は生成されているので、initSpritePanelは行わない。
		if (PhotonNetwork.player.ID == 1) {	// はじめはプレーヤー1だったらパネル生成する！！
			initSpritePanel (false);													// パネル初期化(消えるエフェクトなし=false)
		} else {																		// 今回はPhotonからランダムリストを取得
			// 既に初期化されている場合はrandomList_Vol_Photonからランダム数値を決め打ち
//			int cnt;
//			int tileNum;
//			cnt = gameSysfromVol.randomList_Vol.Count;	// 現在の生成したランダムリストの個数
//			tileNum = gameSysfromVol.randomList_Vol [cnt];
//			initSpritePanel_Photon (tileNum);
		}
	}

	private void initSpritePanel (bool vanishExFlg)
	{		// 消えるエフェクトを表示するかどうかの引数あり
		int test = 0;	// 初期値0
		SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
		while (true) { // 無限ループ
			test = Random.Range (0, maxTileNum);							// これで０から９となるらしい。
			if (gameSysfromVol.randomList_Vol.Count >= 1) {					// タイル２枚目以降
				if (gameSysfromVol.randomList_Vol.Contains (test)) {
					continue; 	// ランダムで出てきた数字がすでに1度出てきてたら、もう一度
				}
			}
			gameSysfromVol.randomList_Vol.Add (test);  						// ランダムで出てきた数字を保持
			//gameSysfromVol.PanelRoot.GetComponent<PhotonDataTrans> ().randomList_Vol_Photon [gameSysfromVol.randomList_Vol.Count - 1] = test;	// Photonのランダム配列へもセット addした個数の配列番号へセット
			renderer.sprite = tiles [test];									// ランダムにスプライト配置

			// パネルが９枚生成されていたなら、もう初期化が完了したのでPhotonのパネル生成完了フラグを立てる
			if (gameSysfromVol.randomList_Vol.Count == 9) {
				//gameSysfromVol.PanelRoot.GetComponent<PhotonDataTrans> ().panelInitFlgPhoton = 1;	// (1:完了)
				// カスタムプロパティセット処理
				int[] iArray = new int[9];
				for (int i = 0; i < iArray.Length; i++) {
					iArray [i] = gameSysfromVol.randomList_Vol [i];
				}
				// ランダムリストを送信
				// パネル削除時に両方が同時にパネル生成をして送信してしまうのを防ぐため今は操作者のみ送信できるようにしている。
				if (gameSysfromVol.PlayerId == PhotonNetwork.player.ID) {				// 今は、クリック者がIDなので一致していると操作者として操作者のみ送信できるようにしている
					gameSysfromVol.customPropertySet (gameSysfromVol.onDragFlg);		// カスタムプロパティセット
				}
			}

			// 消えるエフェクトフラグオンなら表示される（引数）
			if (vanishExFlg == true) {
				GameObject eff = this.transform.FindChild ("Eff_Vanish").gameObject;
				eff.SetActive (false);
				eff.SetActive (true);
			}

			break;	// 一つ生成で終了
		}

		// オーディオソース設定
		audioSource = gameObject.GetComponent<AudioSource> ();
		if (test < (maxTileNum / rowNum)) {
			audioSource.clip = audioClip1;			// 阿澄
		} else if (test < (maxTileNum / rowNum) * 2) {	
			audioSource.clip = audioClip2;			// 加藤
		} else if (test < (maxTileNum / rowNum) * 3) {
			audioSource.clip = audioClip3;			// 原田
		}
		//		Vector3 v3 = this.transform.rotation.eulerAngles;
		//		if (this.transform.rotation == Quaternion.Euler (0, 0, 0)) {
		//			if (test < (maxTileNum / rowNum)) {
		//				audioSource.clip = audioClip1;		// 高音部
		//			} else {
		//				audioSource.clip = audioClip2;		// 低音部
		//			}
		//		} else if (this.transform.rotation == Quaternion.Euler (0, 0, 180)) {
		//			if (test < (maxTileNum / rowNum)) {
		//				audioSource.clip = audioClip3;		// 高音部 リバース
		//			} else {
		//				audioSource.clip = audioClip4;		// 低音部 リバース
		//			}
		//		}
	
	}

	private void initSpritePanel_Photon (int tileNum)
	{		// 消えるエフェクトを表示するかどうかの引数あり
		int test = tileNum;	// 初期値0
		SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
		renderer.sprite = tiles [test];									// ランダムにスプライト配置

		// オーディオソース設定
		audioSource = gameObject.GetComponent<AudioSource> ();
		if (test < (maxTileNum / rowNum)) {
			audioSource.clip = audioClip1;			// 阿澄
		} else if (test < (maxTileNum / rowNum) * 2) {	
			audioSource.clip = audioClip2;			// 加藤
		} else if (test < (maxTileNum / rowNum) * 3) {
			audioSource.clip = audioClip3;			// 原田
		}
	}

	// Photonの情報からパネルを生成する処理
	private void initPanelFromPhoton ()
	{
		// 相手がパネルを生成した場合、そのランダムリストに合わせてこの端末も並び替える処理
		if ((gameSysfromVol.PlayerId != PhotonNetwork.player.ID) && // プレイヤーIDが違う かつ
		    (gameSysfromVol.panelInitFlg == false)) {				// パネル初期化フラグがオフ＝完了している?(Yes)
			if (this.name == "DockImage_Vol_0") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [0]);
			}
			if (this.name == "DockImage_Vol_1") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [1]);
			}
			if (this.name == "DockImage_Vol_2") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [2]);
			}
			if (this.name == "DockImage_Vol_3") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [3]);
			}
			if (this.name == "DockImage_Vol_4") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [4]);
			}
			if (this.name == "DockImage_Vol_5") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [5]);
			}
			if (this.name == "DockImage_Vol_6") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [6]);
			}
			if (this.name == "DockImage_Vol_7") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [7]);
			}
			if (this.name == "DockImage_Vol_8") {
				initSpritePanel_Photon (gameSysfromVol.randomList_Vol [8]);
			}
		}
	}

	// Update is called once per frame
	void Update ()
	{
		// Photonからパネル生成
		initPanelFromPhoton ();

		// 宴表示中の場合はここでリターン
		if (gameSysfromVol.utageDisplayFlg == true) {
			return;
		}
			
		// 一旦揃ったのでパネルを初期化する
		if (gameSysfromVol.panelInitFlg == true) {
			// 初期化完了ならフラグを落としてリターン
			if (gameSysfromVol.randomList_Vol.Count == 9) {
				gameSysfromVol.panelInitFlg = false;													// 初期化フラグを落とす
				//gameSysfromVol.PanelRoot.GetComponent<PhotonDataTrans> ().panelInitFlgPhoton = 1;		// 初期化完了
				return;
			}

			initSpritePanel (true);				// パネル初期化(消えるエフェクトあり=true)
		}


		// 入れ替わったスプライトが存在したらその音を鳴らす
		// 入れ替えた時にサウンドを鳴らさないようにした 2016/12/23
		if (gameSysfromVol.changeSprite != null) {
			SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
			if (renderer.sprite == gameSysfromVol.changeSprite) {
//				// なっている音があれば消す
//				GameObject[] objList = GameObject.FindGameObjectsWithTag ("Volpanel");
//				foreach (GameObject gm in objList) {
//					AudioSource panelAudio = gm.GetComponent<AudioSource> ();	// オーディオソースを取得
//					if (panelAudio.isPlaying == true) {							// 再生中ならば
//						panelAudio.Stop ();										// 停止
//					}
//				}

				// サウンド鳴らす
//				procSoundAndRotate ();		// サウンド再生
				gameSysfromVol.changeSprite = null;	// 入れ替わったスプライトを空にする
			}
		}

		// サウンドの経過時間検出
		if (audioSource.isPlaying == true) {
			elasptime += Time.deltaTime; 		// 経過時間を加算

			if (elasptime >= duration) {		// 経過時間が再生時間を超えた？
				// 再生中のサウンド停止
				procSoundStop ();
			}
		}

		// double tap
		if (isDoubleTapStart) {
			doubleTapTime += Time.deltaTime;
			if (doubleTapTime < 0.3f) {
				if (Input.GetMouseButtonDown (0)) {
					//Debug.Log ("double tap");
					isDoubleTapStart = false;
					doubleTapTime = 0.0f;
				}
			} else {
				//Debug.Log ("reset");
				// reset
				isDoubleTapStart = false;
				doubleTapTime = 0.0f;
			}
		} else {
			if (Input.GetMouseButtonDown (0)) {
				//Debug.Log ("down");
				isDoubleTapStart = true;
			}
		}
	}

	private void procSoundAndRotate ()
	{
		// 反転処理
		//		if (this.transform.rotation == Quaternion.Euler (0, 0, 0)) {
		//			this.transform.rotation = Quaternion.Euler (0, 0, 180);
		//			if (audioSource.clip == audioClip1) {
		//				audioSource.clip = audioClip3;		// 高音部 リバース
		//			} else if(audioSource.clip == audioClip2) {
		//				audioSource.clip = audioClip4;		// 低音部 リバース
		//			}
		//		} else if (this.transform.rotation == Quaternion.Euler (0, 0, 180)) {
		//			this.transform.rotation = Quaternion.Euler (0, 0, 0);
		//			if (audioSource.clip == audioClip3) {
		//				audioSource.clip = audioClip1;		// 高音部
		//			} else if(audioSource.clip == audioClip4) {
		//				audioSource.clip = audioClip2;	// 低音部 
		//			}
		//		}

		// サウンド処理
		SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
		Sprite sp = renderer.sprite;
		for (int i = 0; i < maxTileNum; i++) {
			if (tiles [i].Equals (sp)) {
				// 再生中のサウンド停止
				procSoundStop ();

				// 再生処理
				float auMaxLen = audioSource.clip.length;								// オーディオの長さ
				float wkPosNum = 0;
				if (this.transform.rotation == Quaternion.Euler (0, 0, 0)) {
					//wkPosNum = (maxTileNum / rowNum) - (float)(i % (maxTileNum / rowNum)) - 1;	// 2,1,0を取得する。（列数が３つの場合）逆再生のためwkPosNumも逆にセット
					wkPosNum = (float)(i % (maxTileNum / rowNum));								// 0,1,2を取得する。（列数が３つの場合）順再生のためwkPosNumも順にセット
				}

				audioSource.time = 0;																// はじめから再生
				//audioSource.time = auMaxLen * ((float)wkPosNum / (float)(maxTileNum / rowNum));	// 0,1/3,2/3の位置から再生する。
				audioSource.Play ();

				// 再生時間を取得
				duration = auMaxLen;										// 再生時間セット(全部）
				//duration = (float)( auMaxLen / (maxTileNum / rowNum));	// 再生時間セット（部分的）
				elasptime = 0;											// 経過時間リセット
				break;
			}
		}
	}

	void OnMouseDown ()
	{
		// 宴表示中の場合はここでリターン
		if (gameSysfromVol.utageDisplayFlg == true) {
			return;
		}


		// 今はダブルタップを検知せずにタップ時にそのオブジェクトの音を鳴らす
		//if (isDoubleTapStart) {
		//なっている音があれば消す
		GameObject[] objList = GameObject.FindGameObjectsWithTag ("Volpanel");
		foreach (GameObject gm in objList) {
			AudioSource panelAudio = gm.GetComponent<AudioSource> ();	// オーディオソースを取得
			if (panelAudio.isPlaying == true) {							// 再生中ならば
				panelAudio.Stop ();										// 停止
			}
		}
			
		// サウンド再生処理
		procSoundAndRotate ();
		//}
	}

	// サウンド停止処理（再生中であれば）
	void procSoundStop ()
	{
		//AudioSourceコンポーネントを取得し、変数に格納
		AudioSource[] audioSources = GetComponents<AudioSource> ();
		// 再生中のオーディオは停止する。
		foreach (AudioSource source in audioSources) {
			if (source.isPlaying == true) {
				source.Stop ();
				//source.clip = null;
			}
		}
	}
}

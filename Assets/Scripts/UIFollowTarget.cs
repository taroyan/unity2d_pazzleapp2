﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIFollowTarget : Photon.MonoBehaviour
{
	RectTransform rectTransform = null;
	[SerializeField] Transform target = null;

	void Awake ()
	{
		rectTransform = GetComponent<RectTransform> ();
	}

	void Update ()
	{
		rectTransform.position = RectTransformUtility.WorldToScreenPoint (Camera.main, target.position);
	}
}
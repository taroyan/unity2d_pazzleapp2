﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

	// Listを使うにはこれをインポート必要

public class fftTileSet : Photon.MonoBehaviour
{
	private const int maxTileNum = 9;
	// 最大値9枚
	private const int rowNum = 3;
	// 行数3

	public Sprite[] tiles;
	public GameSystem GSys;
	// GameSystemのオブジェクト
	public AudioClip audioClip1;
	// 阿澄
	public AudioClip audioClip2;
	// 加藤
	public AudioClip audioClip3;
	// 原田
	public AudioSource audioSource;
	// オーディオソース

	private float duration;
	// サウンド再生時間
	private float elasptime;
	// サウンド経過時間
	private bool isDoubleTapStart;
	// ダブルタップ用
	private float doubleTapTime;
	// ダブルタップ用


	// Use this for initialization
	void Start ()
	{
		int test = 0;
		//int test;
		SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
		while (true) { // 無限ループ
			//test = Random.Range (0, maxTileNum);				// これで０から９となるらしい。
			if (GSys.randomList.Count >= 1) {					// タイル２枚目以降
				if (GSys.randomList.Contains (test)) {
					test++;		// 0から順番に並べるために+1する
					continue; 	// ランダムで出てきた数字がすでに1度出てきてたら、もう一度
				}
			}
			GSys.randomList.Add (test);  // ランダムで出てきた数字を保持
			//renderer.sprite = tiles [test];

			break;	// 一つ生成で終了
		}

		// オーディオソース設定(再生クリップは元の設定のまま）
		audioSource = gameObject.GetComponent<AudioSource> ();
		if (test < (maxTileNum / rowNum)) {
			audioSource.clip = audioClip1;			// 阿澄
		} else if (test < (maxTileNum / rowNum) * 2) {	
			audioSource.clip = audioClip2;			// 加藤
		} else if (test < (maxTileNum / rowNum) * 3) {
			audioSource.clip = audioClip3;			// 原田
		}
//		Vector3 v3 = this.transform.rotation.eulerAngles;
//		if (this.transform.rotation == Quaternion.Euler (0, 0, 0)) {
//			if (test < (maxTileNum / rowNum)) {
//				audioSource.clip = audioClip1;		// 高音部
//			} else {
//				audioSource.clip = audioClip2;		// 低音部
//			}
//		} else if (this.transform.rotation == Quaternion.Euler (0, 0, 180)) {
//			if (test < (maxTileNum / rowNum)) {
//				audioSource.clip = audioClip3;		// 高音部 リバース
//			} else {
//				audioSource.clip = audioClip4;		// 低音部 リバース
//			}
//		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		// 入れ替わったスプライトが存在したらその音を鳴らす
		if (GSys.changeSprite != null) {
			SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
			if (renderer.sprite == GSys.changeSprite) {
				// なっている音があれば消す
				GameObject[] objList = GameObject.FindGameObjectsWithTag ("FFTpanel");
				foreach (GameObject gm in objList) {
					AudioSource panelAudio = gm.GetComponent<AudioSource> ();	// オーディオソースを取得
					if (panelAudio.isPlaying == true) {							// 再生中ならば
						panelAudio.Stop ();										// 停止
					}
				}

				// サウンド鳴らす
				procSoundAndRotate ();		// サウンド再生
				GSys.changeSprite = null;	// 入れ替わったスプライトを空にする
			}
		}


		// サウンドの経過時間検出
		if (audioSource.isPlaying == true) {
			elasptime += Time.deltaTime; 		// 経過時間を加算

			if (elasptime >= duration) {		// 経過時間が再生時間を超えた？
				// 再生中のサウンド停止
				procSoundStop ();
			}
		}

		// double tap
		if (isDoubleTapStart) {
			doubleTapTime += Time.deltaTime;
			if (doubleTapTime < 0.3f) {
				if (Input.GetMouseButtonDown (0)) {
					//Debug.Log ("double tap");
					isDoubleTapStart = false;
					doubleTapTime = 0.0f;
				}
			} else {
				//Debug.Log ("reset");
				// reset
				isDoubleTapStart = false;
				doubleTapTime = 0.0f;
			}
		} else {
			if (Input.GetMouseButtonDown (0)) {
				//Debug.Log ("down");
				isDoubleTapStart = true;
			}
		}
	}

	private void procSoundAndRotate ()
	{
		// 反転処理
//		if (this.transform.rotation == Quaternion.Euler (0, 0, 0)) {
//			this.transform.rotation = Quaternion.Euler (0, 0, 180);
//			if (audioSource.clip == audioClip1) {
//				audioSource.clip = audioClip3;		// 高音部 リバース
//			} else if(audioSource.clip == audioClip2) {
//				audioSource.clip = audioClip4;		// 低音部 リバース
//			}
//		} else if (this.transform.rotation == Quaternion.Euler (0, 0, 180)) {
//			this.transform.rotation = Quaternion.Euler (0, 0, 0);
//			if (audioSource.clip == audioClip3) {
//				audioSource.clip = audioClip1;		// 高音部
//			} else if(audioSource.clip == audioClip4) {
//				audioSource.clip = audioClip2;	// 低音部 
//			}
//		}

		// サウンド処理
		SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
		Sprite sp = renderer.sprite;
		for (int i = 0; i < maxTileNum; i++) {
			if (tiles [i].Equals (sp)) {
				// 再生中のサウンド停止
				procSoundStop ();

				// 再生処理
				float auMaxLen = audioSource.clip.length;								// オーディオの長さ
				float wkPosNum = 0;
				if (this.transform.rotation == Quaternion.Euler (0, 0, 0)) {
					//wkPosNum = (maxTileNum / rowNum) - (float)(i % (maxTileNum / rowNum)) - 1;	// 2,1,0を取得する。（列数が３つの場合）逆再生のためwkPosNumも逆にセット
					wkPosNum = (float)(i % (maxTileNum / rowNum));									// 0,1,2を取得する。（列数が３つの場合）順再生のためwkPosNumも順にセット
				}
				audioSource.time = auMaxLen * ((float)wkPosNum / (float)(maxTileNum / rowNum));	// 2/3,1/3,0の位置から再生する。
				audioSource.Play ();

				// 再生時間を取得
				duration = (float)(auMaxLen / (maxTileNum / rowNum));	// 再生時間セット
				elasptime = 0;											// 経過時間リセット
				break;
			}
		}
	}

	void OnMouseDown ()
	{
		if (isDoubleTapStart) {
			procSoundAndRotate ();		// サウンド再生
		}	
	}

	// サウンド停止処理（再生中であれば）
	void procSoundStop ()
	{
		//AudioSourceコンポーネントを取得し、変数に格納
		AudioSource[] audioSources = GetComponents<AudioSource> ();
		// 再生中のオーディオは停止する。
		foreach (AudioSource source in audioSources) {
			if (source.isPlaying == true) {
				source.Stop ();
				//source.clip = null;
			}
		}
	}
}

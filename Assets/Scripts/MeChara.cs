﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MeChara : Photon.MonoBehaviour
{
	public Animator animator_Enemy;
	// テキストスライドインアニメーター（敵キャラ）
	public Sprite	enemyNormal;
	// 敵キャラ通常
	public Sprite	enemyDamage;
	// 敵キャラダメージ
	public Sprite	enemyAttack;
	// 敵キャラ攻撃
	public AudioClip[]	audioClip;
	// オーディオクリップ配列
	public GameObject enemyChara;
	// 敵キャラオブジェクト
	public Sprite	heroinDamage;
	// 主人公キャラダメージ
	public Sprite	heroinNormal;
	// 主人公キャラ通常
	public Animator animator_MeChara;
	// テキストスライドインアニメーター（自分キャラ）
	public GameObject	hitText;
	// 敵のヒットテキストオブジェクト（canvas内にある）
	public GameSystem gSys;
	// メインのゲームシステム(スクリプトのクラス）

	// Use this for initialization
	void Start ()
	{
		//animator_MeChara.SetBool ("IsMeCharaHit", false);	// 一旦falseにしてtrueで遷移（いいやり方別にあるはずだが）
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void showEndMeCharaVib ()
	{
		//テキストにEndを入れる
		//textObj.GetComponent<Text> ().text = "End";
		animator_MeChara.SetBool ("IsMeCharaHit", false);	// 一旦falseにしてtrueで遷移（いいやり方別にあるはずだが）

		// スプライトを元に戻す
		// 敵キャラ
		SpriteRenderer nowRenderer = enemyChara.GetComponent<SpriteRenderer> ();	// 敵キャラのスプライトレンダラー
		nowRenderer.sprite = enemyNormal;									// 通常へ

		// 主人公キャラ
		SpriteRenderer heroinRenderer = this.GetComponent<SpriteRenderer> ();	// 主人公キャラのスプライトレンダラー
		heroinRenderer.sprite = heroinDamage;										// ダメージ顔へ

		// ヒットテキストを非表示にする（アニメーション終了時の関数＝この関数で非表示）
		hitText.SetActive (false);
	}

	public void proc_ChangeMeCharaSpriteToDamege (int fromEnemyAtkType)
	{
		int hitDamage = 0;

		//int hitCnt = gSys.hitCntOneAtk;
		if (fromEnemyAtkType == 0) {
			hitDamage = (int)(gSys.oneDamageMe1 / gSys.oneDamageEnemy) * 100;		// ダメージ　(-0.15f / -0.005) * 100 = 3000 
		} else if (fromEnemyAtkType == 1) {
			hitDamage = (int)(gSys.oneDamageMe2 / gSys.oneDamageEnemy) * 100;		// ダメージ　（-0.05f / -0.005f) * 100 = 1000
		} else if (fromEnemyAtkType == 2) {
			hitDamage = ((int)(gSys.oneDamageMe1 / gSys.oneDamageEnemy) * 100)// ダメージ　(-0.15f / -0.005) * 100 = 3000
			+ ((int)(gSys.oneDamageMe2 / gSys.oneDamageEnemy) * 100);			// ダメージ　（-0.05f / -0.005f) * 100 = 1000
		}
		// 敵キャラ
		SpriteRenderer enemyRenderer = enemyChara.GetComponent<SpriteRenderer> ();	// 敵キャラのスプライトレンダラー
		enemyRenderer.sprite = enemyAttack;									// 敵の攻撃スプライト
//
//		// 主人公キャラ
//		SpriteRenderer nowRenderer = this.GetComponent<SpriteRenderer> ();	// 主人公キャラのスプライトレンダラー
//		nowRenderer.sprite = heroinNormal;										// 通常顔へ
//
		// サウンド再生処理
		proc_PlaySound ();

		// ヒットテキストを表示させる（アニメーション終了時の関数で非表示）
		hitText.GetComponent<Text> ().text = "ATTACK!! " + hitDamage.ToString ();	// 文字　例「2 HIT!! 200」
		hitText.SetActive (true);
	}


	// サウンド再生処理
	private void proc_PlaySound ()
	{
		AudioSource audioSrc = this.GetComponent<AudioSource> ();
		int inx = Random.Range (0, 4);					// これで０から３となるらしい。（４は含まないらしい）

		audioSrc.clip = audioClip [inx];				// ランダムな音声

		audioSrc.Play ();
	}

}

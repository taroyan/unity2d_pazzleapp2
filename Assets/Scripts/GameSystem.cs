﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Listを使うにはこれをインポート必要
using Utage;
using UnityEngine.UI;

public class GameSystem : Photon.MonoBehaviour
{
	public GameSystem GSys;
	// GameSystemのオブジェクト
	public PhotonManager PtnMng;
	// PhotonManagerオブジェクト
	public GameObject PanelRoot;
	// Photonのパネルをまとめたパネルルートのオブジェクト
	public float oneDamageEnemy = -0.005f;
	// 敵への一回分のダメージ（フルダメージは0.5f)
	public float oneDamageMe1 = -0.15f;
	// 自分がくらうダメージ(一定攻撃）
	public float oneDamageMe2 = -0.05f;
	// 自分がくらうダメージ(パネルゼロ攻撃）

	public GameObject holdObj;
	// ホールドオブジェクト
	public GameObject dataRune;
	// 魔法陣
	public float holdPositionX;
	// クリックしたときのオブジェクトのX座標
	public float holdPositionY;
	// クリックしたときのオブジェクトのY座標

	public AudioClip audioClip1;
	// 阿澄
	public AudioClip audioClip2;
	// 加藤
	public AudioClip audioClip3;
	// 原田
	public AudioClip audioClip4;
	// 原
	public AudioClip audioClip5;
	// パネル移動サウンド

	public List<int> randomList = new List<int> ();
	// ランダムに選ばれたタイルNo(FFT)
	public List<int> randomList_Vol = new List<int> ();
	// ランダムに選ばれたタイルNo(Vol)
	public Sprite[] correctTilesFFT;
	// 正しいタイルの並び(FFT)
	public Sprite[] correctTilesVol;
	// 正しいタイルの並び(Vol)
	public Sprite[] clearTilesFFT1;
	// 正しいタイルの並び(FFT)
	public Sprite[] clearTilesVol1;
	// 正しいタイルの並び(Vol)
	public Sprite[] clearTilesFFT2;
	// 正しいタイルの並び(FFT)
	public Sprite[] clearTilesVol2;
	// 正しいタイルの並び(Vol)
	public Sprite[]	charaList;
	// キャラ（スタートとクリア）
	public Sprite	changeSprite;
	// 入れ替わったスプライト保持用
	public GameObject charaImage;
	// キャライラストオブジェクト
	public Text ClearText;
	// くりあ〜文字
	public Text ShiftCntText;
	// 移動回数テキスト
	public AudioSource audioSource;
	// オーディオソース
	public Text time_Limit;
	// 残り時間
	public GameObject fireParticle;
	// ファイヤーパーティクルオブジェクト
	public GameObject thunderEx;
	// サンダーエフェクトオブジェクト
	public GameObject stormEx;
	// ストームオブジェクト

	public GameObject dummyParticleObj;
	// ダミーのパーティクルオブジェクト
	public bool photonInitiateFlg;
	// Photonでパネル生成フラグ（True:生成完了）
	public bool utageDisplayFlg;
	// 宴が表示中かどうかのフラグ（True:表示中）
	public bool onClickChk;
	// 左クリックしてクリック時の処理を完了したかどうかフラグ（false:未完了　true:完了）
	public int onDragFlg;
	// ドラッグ中かどうかのフラグ（0:ドラッグしてない　1:ドラッグした瞬間（クリック時）　2:ドラッグ中　3:ドラッグから手を離した）
	public float xClickPos;
	// クリック時のXポジション
	public float yClickPos;
	// クリック時のYポジション
	public Canvas canvas;
	// キャンバス（ボタンとか文字が表示されているオブジェクト）
	public GameObject PGauge1;
	// パワーゲージ1
	public GameObject PGauge2;
	// パワーゲージ2
	public int	hitCntOneAtk;
	// 一回の攻撃でヒットした回数（パネルを消した行数）
	public bool panelInitFlg;
	// パネルを初期化するかのフラグ（一旦消したあとにTrueになる）
	public GameObject AtkTimesText;
	// 敵の攻撃までの回数表示テキスト
	public int MoveTimesPanel;
	// タイルを移動させた回数
	public int MoveTimesPanel_Hoji;
	// タイルを移動させた回数（敵キャラダメージ表示用保持変数）
	public GameObject MoveTimesText;
	// タイルを移動させた回数表示テキスト
	public GameObject enemyChara;
	// 敵キャラのゲームオブジェクト
	public GameObject meChara;
	// 自分キャラのゲームオブジェクト
	public int enemyAtkType;
	// 敵キャラの攻撃タイプ
	public int meAtkType;
	// 自キャラの攻撃タイプ

	// アニメーション系変数****
	public bool EnemyHitFlg;
	// 敵キャラ振動フラグ
	public bool MeCharaHitFlg;
	// 自分キャラ振動フラグ

	// *********************

	// メインカメラ系変数******
	public GameObject mc;
	// メインカメラ
	// *********************

	// 宴系変数
	public AdvEngine engine;
	public string scenarioLabel;

	// *******Photon系パブリック変数**********
	public int PlayerId;
	// パネルを生成したプレイヤーのID (つまり生成後次は相手のターンに移る）

	// ************************************

	public GameObject test;

	// **********プライベート変数************
	private const int TileLineNum = 3;
	// 最大タイル行数
	private const int TileColNum = 3;
	// 最大タイル列数
	private const float TileWidth = 2.0f;
	// タイル幅
	private const float TileHeight = 2.0f;
	// タイル高さ
	private const float FirstTilePosX = -4.0f;
	// 左下のタイルのX座標（タイルの中心）
	private const float FirstTilePosY = -2.0f;
	// 左下のタイルのY座標（タイルの中心）
	private const float FirstTilePosX_R = -2.0f;
	// 左下のタイルのX座標（タイルの中心）
	private const float FirstTilePosY_R = -2.0f;
	// 左下のタイルのY座標（タイルの中心）
	private const int MovePanelMax = 20;
	// パネルの最大移動回数（初期値）
	private float z = 10f;
	// Z軸
	private GameObject[,] tileSet_L;
	// 左のタイルセット
	private GameObject[,] tileSet_R;
	// 右のタイルセット
	private bool clearOnceFlg = false;
	// 一回クリアしたかどうかのフラグ
	private float remainingTime = 300.0f;
	// プレイ時間のリミット（３００秒）
	private bool[] rowClearList = new bool[TileLineNum];
	// 行クリア時の再生ボイス流したかのフラグ
	private bool[,] clearList = new bool[TileLineNum, 2];
	// 成功フラグ（行ごと）2はFFTとVOL
	private const int shiftMax = 3;
	// 敵攻撃までの回数
	private int shiftNum = 0;
	// 移動回数
	private bool shiftFlg = false;
	// オブジェクトの入れ替わりがあったかのフラグ（あれば移動開始したと判断）
	[SerializeField]
	private bool leftDrugFlg = false;
	// ドラッグ中かどうかのフラグ
	private bool endingFlg = false;
	// エンディングフラグ
	private float timeLeftFireEx = 0.0f;
	// 炎エフェクトの表示時間
	private float timeMovePanel = 0.0f;
	// パネル移動回数のリセット表示時間（1.0なら１秒）
	private float timeEnemyAtkCnt = 0.0f;
	// 敵のアタックカウント表示時間（1.0なら１秒）

	// **********デバッグ変数***********
	private float dbg1;
	// デバッグ変数1

	// **********Photon系変数**********
	private GameObject panel_All;
	// パネル全てをまとめたルート
	private GameObject cube_test;
	// キューブの同期テスト


	// *************************Photonカスタムプロパティテスト************************
	[SerializeField]
	private Text m_strategy = null;
	// デバッグ用１
	[SerializeField]
	private Text m_debug = null;
	// デバッグ用２
	
	private Color[] PLAYER_COLOR = new Color[] { Color.white, Color.red, Color.green, Color.blue, Color.yellow };
	
	// Photonカスタムプロパティをセット
	public void SetRoomStrategy (string i_strategy, int[] panel_list, int movTimes, int onDrag, string hObjname, float xhold, float yhold)
	{
		var properties = new ExitGames.Client.Photon.Hashtable ();
		properties.Add ("Strategy", i_strategy);					// 単なる文字列
		properties.Add ("Sender", PhotonNetwork.player.ID);			// これをターンとする。1か2   パネル生成者は相手にターンをゆずっているので、PlayerId != PhotonNetwork.player.ID)の場合、その端末がターン
		properties.Add ("PanelList", panel_list);					// ランダムリスト
		properties.Add ("PanelMoveTimes", movTimes);				// パネルの移動回数
		properties.Add ("OnDragFlg", onDrag);						// ドラッグ中フラグ
		properties.Add ("xPos", Input.mousePosition.x);				// マウスポジションX
		properties.Add ("yPos", Input.mousePosition.y);				// マウスポジションY
		properties.Add ("HoldObjectName", hObjname);				// 今ドラッグ中のオブジェクトを相手へ渡す
		properties.Add ("HoldXPos", xhold);							// 今ドラッグ中のオブジェクトを相手へ渡す
		properties.Add ("HoldYPos", yhold);							// 今ドラッグ中のオブジェクトを相手へ渡す
	
		PhotonNetwork.room.SetCustomProperties (properties);
	}
	
	// Photonカスタムプロパティをセット時に別端末で呼ばれる関数
	public void OnPhotonCustomRoomPropertiesChanged (ExitGames.Client.Photon.Hashtable i_propertiesThatChanged)
	{
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("Strategy", out value)) {
				m_strategy.text = (string)value;
			}
	
		}
	
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("Sender", out value)) {
				m_strategy.text = value.ToString ();
				PlayerId = (int)value;					// プレイヤーIDを保存
			}
	
		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("PanelList", out value)) {
				if (PlayerId != PhotonNetwork.player.ID) {	// 保存しているプレイヤーIDと自分のプレイヤーIDが違う?(つまり相手がパネルを生成して自分のターンになっている）
					int[] iArray = (int[])value;
					//randomList_Vol [0] = (int)value;			// ランダムリストをセット
					randomList_Vol.Clear ();					// ランダムリストを一旦クリア
					for (int i = 0; i < iArray.Length; i++) {
						m_strategy.text += iArray [i].ToString ();
						randomList_Vol.Add (iArray [i]);		// リストを追加
					}
				}
			}
		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("PanelMoveTimes", out value)) {
				if (PlayerId != PhotonNetwork.player.ID) {			// クリック者と違う?(Yes) =つまりドラッグしていない方
					MoveTimesPanel = (int)value;					// プレイヤーIDを保存
				}
			}

		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("OnDragFlg", out value)) {
				if (onDragFlg == 0) {					// 現在ドラッグ中でない？(Yes)
					onDragFlg = (int)value;				// ドラッグ中フラグを受け渡し
					m_debug.text = "Non Drag";			// ドラッグしてない
				} else if (onDragFlg == 1) {			// ドラッグした瞬間（クリック時）？(Yes)
					onDragFlg = (int)value;				// ドラッグ中フラグを受け渡し
					m_debug.text = "Click";				// クリック時
				} else if (onDragFlg == 2) {			// 現在ドラッグ中?(Yes)
					if ((int)value != 0) {				// ０以外?(Yes) = ドラッグしていない状態ではない場合
						onDragFlg = (int)value;			// ドラッグ中フラグを受け渡し(１か２か３しか受け付けない）
						m_debug.text = "Drag";			// ドラッグ中
					}
				}

				// onDragFlgが2のときに3にかわる可能性があるためelseifにはせずにこれだけ外す
				if (onDragFlg == 3) {					// ドラッグから手を離した？(Yes)
					procAction (onDragFlg, true);		// アクション実行(3が引数で渡り、ドラッグから手を離したときの処理が行われる） Photonからの書き換えなので第二引数はtrue
					onDragFlg = 0;						// 受け渡しの情報ではなく、直接０へ落とす
					m_debug.text = "Drag off";			// ドラッグ解除
				}
			}
		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("xPos", out value)) {
				if (onDragFlg == 1) {
					// Xを保存
					xClickPos = (float)value;
				}
			}
		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("yPos", out value)) {
				if (onDragFlg == 1) {
					// Yを保存
					yClickPos = (float)value;
				}
			}
		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("HoldObjectName", out value)) {
				if (PlayerId != PhotonNetwork.player.ID) {			// クリック者と違う?(Yes) =つまりドラッグしていない方
					if (onDragFlg != 0) {							// 何もしていない状態ではない?(Yes)
						// ドラッグ中のオブジェクトをセット
						string hName = value.ToString ();
						if (hName != "non") {
							GameObject hoj = GameObject.Find (hName);
							holdObj = hoj;								// 今ドラッグ中のオブジェクトをセット
						}
					}
				}
			}
		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("HoldXPos", out value)) {
				if (PlayerId != PhotonNetwork.player.ID) {			// クリック者と違う?(Yes) =つまりドラッグしていない方
					// ドラッグ中のオブジェクトをセット
					if (holdObj != null) {
						holdPositionX = (float)value;
					}
				}
			}
		}
		{
			object value = null;
			if (i_propertiesThatChanged.TryGetValue ("HoldYPos", out value)) {
				if (PlayerId != PhotonNetwork.player.ID) {			// クリック者と違う?(Yes) =つまりドラッグしていない方
					// ドラッグ中のオブジェクトをセット
					if (holdObj != null) {
						holdPositionY = (float)value;
					}
				}
			}
		}
	}
	// ***************************************************************************


	// Use this for initialization
	void Start ()
	{
		// *********ここが全てのスタート地点！！************
		// まずはPhotonへ接続してロビールーム入室＆パネル生成まで
		photonInitiateFlg = false;		// 初期化（未生成）生成後はPhotonManeger内で更新させる）
		ConnectPhoton ();				// Photonへコネクト開始
	}
		
	// フォトンへ接続　（ボタンから呼び出し＆Start内で自動呼び出し）
	public void ConnectPhoton ()
	{
		// これを実行するとロビーに入るらしい。（unityのプロパティでAuto-Join Lobbyに設定しているため）
		PhotonNetwork.ConnectUsingSettings ("v1.0");

		// 適当にニックネームを設定
		PhotonNetwork.playerName = "guest" + UnityEngine.Random.Range (1000, 9999);
	}

	// ロビーに入ったときに呼ばれるコールバック関数
	public void OnJoinedLobby ()
	{
		Debug.Log ("PhotonManager OnJoinedLobby");
		// ボタンを押せるようにする
		GameObject.Find ("Button_RoomIn").GetComponent<Button> ().interactable = true;
		// 同時に今はCreateRoomも自動で行う。
		CreateRoom ();

	}

	// ルーム作成  (ボタンプッシュで呼び出し）
	public void CreateRoom ()
	{
		string userName = "ユーザ1";
		string userId = "user1";
		PhotonNetwork.autoCleanUpPlayerObjects = false;
		//カスタムプロパティ
		ExitGames.Client.Photon.Hashtable customProp = new ExitGames.Client.Photon.Hashtable ();
		customProp.Add ("userName", userName); //ユーザ名
		customProp.Add ("userId", userId); //ユーザID
		PhotonNetwork.SetPlayerCustomProperties (customProp);

		RoomOptions roomOptions = new RoomOptions ();
		roomOptions.customRoomProperties = customProp;
		//ロビーで見えるルーム情報としてカスタムプロパティのuserName,userIdを使いますよという宣言
		roomOptions.customRoomPropertiesForLobby = new string[]{ "userName", "userId" };
		roomOptions.maxPlayers = 2; //部屋の最大人数
		roomOptions.isOpen = true; //入室許可する
		roomOptions.isVisible = true; //ロビーから見えるようにする
		//userIdが名前のルームがなければ作って入室、あれば普通に入室する。
		PhotonNetwork.JoinOrCreateRoom (userId, roomOptions, null);

	}

	// ルーム入室した時に呼ばれるコールバックメソッド（OnJoinedRoom はどうもローカルプレイヤーのみに反応しているよう）
	public void OnJoinedRoom ()
	{
		Debug.Log ("PhotonManager OnJoinedRoom");
		GameObject.Find ("StatusText").GetComponent<Text> ().text = "OnJoinedRoom";
		UpdateMemberList ();						// メンバーを見て、パネル生成を行う。
	}

	// <summary>
	// リモートプレイヤーが入室した際にコールされる
	// </summary>
	public void OnPhotonPlayerConnected (PhotonPlayer player)
	{
		Debug.Log (player.name + " is joined.");
		// 一旦はずす
		// UpdateMemberList ();						// メンバーを見て、パネル生成を行う。
	}

	// メンバーを見て、パネル生成を行う。
	public void UpdateMemberList ()
	{
		foreach (var p in PhotonNetwork.playerList) {
			// 何もしない
		}
			
		if (PhotonNetwork.playerList.Length == 1) {			// ルームに自分しかいない＝１の場合は、パネル生成
			Vector3 initPos = new Vector3 (0.0f, 0.0f, 0.0f);
			if (GameObject.Find ("PanelRoot") == null) {
				panel_All = PhotonNetwork.Instantiate ("PanelRoot", initPos, Quaternion.Euler (Vector3.zero), 0);	// オブジェクト生成(9枚のパネルまとめて）

				//Vector3 initPos2 = new Vector3 (4.26f, 0.52f, -7.35f);
				//cube_test = PhotonNetwork.Instantiate ("Cube", initPos2, Quaternion.Euler (Vector3.zero), 0);		// キューブの同期テスト
				//  自分が生成したPlayerを移動可能にする
				//cube_test.GetComponent<PlayerMove> ().enabled = true;

				// *******Photon同期変数初期化**********
				for (int i = 0; i < PanelRoot.GetComponent<PhotonDataTrans> ().randomList_Vol_Photon.Length; i++) {
					PanelRoot.GetComponent<PhotonDataTrans> ().randomList_Vol_Photon [i] = 9;		// 初期値9 セット
				}
				PanelRoot.GetComponent<PhotonDataTrans> ().panelInitFlgPhoton = 0;
				// ***********************************

				// パネル生成したら、Photonフラグをオンへ
				photonInitiateFlg = true;

			}
		} else {											// 自分以外に既に人がいた場合
			// パネル生成が完了しているので、フラグを立てるだけ。
			photonInitiateFlg = true;
			// 今はフラグも立てず、InitStart()も行わない。
		}
	}

	// ルーム一覧が取れると　　非同期で入室したときにこの関数がコールバックされる。
	void OnReceivedRoomListUpdate ()
	{
		//ルーム一覧を取る
		RoomInfo[] rooms = PhotonNetwork.GetRoomList ();
		if (rooms.Length == 0) {
			Debug.Log ("ルームが一つもありません");
		} else {
			//ルームが1件以上ある時ループでRoomInfo情報をログ出力
			for (int i = 0; i < rooms.Length; i++) {
				Debug.Log ("RoomName:" + rooms [i].name);
				Debug.Log ("userName:" + rooms [i].customProperties ["userName"]);
				Debug.Log ("userId:" + rooms [i].customProperties ["userId"]);
				GameObject.Find ("StatusText").GetComponent<Text> ().text = rooms [i].name;
			}
		}
	}

	IEnumerator CoTalk ()
	{
		//「宴」のシナリオを呼び出す
		engine.JumpScenario (scenarioLabel);

		//「宴」のシナリオ終了待ち
		while (!engine.IsEndScenario) {
			yield return 0;
		}
	}

	// 立ち上げ時初期化処理（Update内で一度だけ使用）
	public void InitStart ()
	{
		//SetTileSet_LStart ();	// タイルのオブジェクトを取得する(左側)
		SetTileSet_RStart ();	// タイルのオブジェクトを取得する(右側)

		// 敵の攻撃までの回数セット
		//ShiftCntText.GetComponent<Text> ().text = " " + (shiftMax - shiftNum).ToString () + "回";		// 残り回数
		AtkTimesText.GetComponent<TextMesh> ().text = " " + (shiftMax - shiftNum).ToString ();		// 残り回数

		// パネル移動回数を初期化
		MoveTimesPanel_Hoji = MovePanelMax;
		MoveTimesPanel = MovePanelMax;
		MoveTimesText.GetComponent<TextMesh> ().text = " " + (MoveTimesPanel).ToString ();		// 最大移動回数

		// オーディオソース設定
		audioSource = gameObject.GetComponent<AudioSource> ();

		// 行成功フラグ初期化
		for (int i = 0; i < TileLineNum; i++) {
			rowClearList [i] = false;
		}

		// 成功フラグ初期化
		for (int i = 0; i < TileLineNum; i++) {
			for (int j = 0; j < 2; j++) {
				clearList [i, j] = false;
			}
		}

		// 宴呼び出し
		StartCoroutine (CoTalk ());
		utageDisplayFlg = true;			// ここで宴プレイ中フラグオン

		// キャンバス一旦オフ（宴が消えたらオンにする）
		canvas.enabled = false;

		// メインカメラのimage effectをオン		別のネームスペースへアクセスしないといけない。
		mc.gameObject.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain> ().enabled = true;
		mc.gameObject.GetComponent<UnityStandardAssets.ImageEffects.Blur> ().enabled = true;
	}

	// Update is called once per frame
	void Update ()
	{
		// Photon生成フラグがTrueならば一回だけ実行
		if (photonInitiateFlg == true) {
			InitStart ();
			photonInitiateFlg = false;
		}

		if (Input.GetMouseButtonDown (0)) {	// 左クリックをした瞬間
			// ここで、xとyの情報をカスタムプロパティへセットする。
			onDragFlg = 1;					// クリック時
			customPropertySet (onDragFlg);	// カスタムプロパティセット
			procAction (onDragFlg, false);			// アクション実行
		} else if (Input.GetMouseButton (0)) {		// 左クリックをドラッグ
			if (Input.touchCount <= 1) {	// タッチカウントが１つ（＝２重でタッチしていない）ならばOK
				onDragFlg = 2;				// ドラッグ中
				customPropertySet (onDragFlg);	// カスタムプロパティセット
				procAction (onDragFlg, false);			// アクション実行
			}
		} else if (Input.GetMouseButtonUp (0)) {	// 左クリックを離したとき
			onDragFlg = 3;					// フラグオフ		ここで3にしたら、カスタムプロパティのコールバックでのみ０へ落とされる
			customPropertySet (onDragFlg);	// カスタムプロパティセット
			// 離したときのアクションは、カスタムプロパティのonDragFlg受信時に処理しているのでここでは行わない。
			//procAction (onDragFlg);			// アクション実行
		}

		// 毎フレームで常にアクション実行（onDragFlgが0のときは何もしない）
		procAction (onDragFlg, false);			// アクション実行
			
		// タイマー系の処理
		timerManager ();

	}

	// Update関数内の実際の処理
	private void procAction (int dragFlg, bool fromPhoton)
	{
		
		// ドラッグ時の処理を実行
		if (onDragFlg == 0) {			// 何もしていないとき
			// 何もしない
		} else if (onDragFlg == 1) {	// クリック時
			LeftClick ();				// クリック時処理
			onClickChk = true;			// クリック時処理完了フラグオン
		} else if (onDragFlg == 2) {	// ドラッグ中
			if (onClickChk == false) {	// クリック時処理完了フラグがオンでない？(Yes)
				LeftClick ();			// クリック時処理
				onClickChk = true;		// クリック時処理完了フラグオン
			}
			LeftDrag ();				// ドラッグ時処理
		} else if (onDragFlg == 3) {	// ドラッグから手を離したとき
			if (fromPhoton == true) {	// Photonのカスタムプロパティ変更時の呼び出し  = つまりupdate関数からは呼ばない
				LeftUp ();				// 離した時の処理
				DeleteMatchTile ();		// タイルの一致チェック
			}
			onClickChk = false;			// クリック時処理完了フラグオフ
		}
	}

	// カスタムプロパティセット関数
	public void customPropertySet (int dragStatus)
	{
		//if (PlayerId != PhotonNetwork.player.ID) {			// 現在のプレーヤと端末のプレーヤが違う?(Yes) つまり自分のターン中
			
		int[] iArray = new int[9];
		// カスタムプロパティがList使えないから無理やり配列にしている
		for (int i = 0; i < iArray.Length; i++) {
			iArray [i] = randomList_Vol [i];
		}

		string dum;					// ホールドオブジェクトのダミー
		if (holdObj != null) {
			dum = holdObj.name;
		} else {
			dum = "non";
		}

		SetRoomStrategy ("Player2", iArray, MoveTimesPanel, dragStatus, dum, holdPositionX, holdPositionY);		// カスタムプロパティのセットテスト

		//}
	}

	private void LeftClick ()
	{

		// 宴表示中はリターン
		if (utageDisplayFlg == true) {
			return;
		}

		if (PlayerId != PhotonNetwork.player.ID) {			// クリック者以外はリターン
			return;
		}

		// パネル初期化処理（ドラッグから手を離したときとクリック開始時に実行）
		if (onClickChk == false) {			// クリックチェックがfalse＝１回目のクリック時の場合のみ
			SetPanelInitLeaveDrag ();
		}

		Vector3 tapPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, z);
		// Vector3 tapPoint = new Vector3 (xClickPos, yClickPos, z);
		if (holdObj == null) {
			Collider2D col = Physics2D.OverlapPoint (Camera.main.ScreenToWorldPoint (tapPoint));
			if (col != null) {
				// holdOjbがFFTpanelの場合はリターン つまり動かさない
				if ("FFTpanel".Equals (col.tag)) {
					return;
				}

				this.holdObj = col.gameObject;
				holdPositionX = this.holdObj.transform.position.x;
				holdPositionY = this.holdObj.transform.position.y;
				holdObj.transform.position = Camera.main.ScreenToWorldPoint (tapPoint);
			}
		}


		// Photonオブジェクトのオーナーをこの端末へ変更
		GameObject.Find ("DockImage_Vol_0").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_1").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_2").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_3").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_4").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_5").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_6").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_7").GetComponent<PhotonView> ().RequestOwnership ();
		GameObject.Find ("DockImage_Vol_8").GetComponent<PhotonView> ().RequestOwnership ();

	}

	private void timerManager ()
	{
	
		float deltaTime = Time.deltaTime;	// 時間差分

		// 炎エフェクト
		//だいたい2秒ごとに処理を行う
		if (timeLeftFireEx > 0.0) {
			timeLeftFireEx -= deltaTime;
			if (timeLeftFireEx <= 0.0) {
				setParticles (false, 0);	// エフェクトを消す
			}
		}

		// パネル移動回数
		// 一定時間表示し終わったら元の数字に戻す
		if (timeMovePanel > 0.0) {
			timeMovePanel -= deltaTime;
			if (timeMovePanel <= 0.0) {
				MoveTimesText.GetComponent<TextMesh> ().fontSize = 500;									// 500へもどす
				MoveTimesText.GetComponent<TextMesh> ().text = " " + (MoveTimesPanel).ToString ();		// 現在の移動回数
			}
		}		
		// 敵のアタックまでの回数
		// 一定時間表示し終わったら元の数字に戻す
		if (timeEnemyAtkCnt > 0.0) {
			timeEnemyAtkCnt -= deltaTime;
			if (timeEnemyAtkCnt <= 0.0) {
				AtkTimesText.GetComponent<TextMesh> ().fontSize = 500;									// 500へもどす
				AtkTimesText.GetComponent<TextMesh> ().text = " " + (shiftMax - shiftNum).ToString ();		// 残り回数
			}
		}


		// 制限時間の処理
		remainingTime -= deltaTime;
		time_Limit.text = remainingTime.ToString ("N1");	// 残り時間表示

		// ゲームオーバー
		if (remainingTime <= 0) {			// 0秒以下になると終了(いまはしないで再び300秒セット）
			// シーンを再読み込み
			//Application.LoadLevel("main");
			// UnityEngine.SceneManagement.SceneManager.LoadScene("main");
			remainingTime = 300;	// 再セット
		}
	}

	// 左ドラッグ中
	private void LeftDrag ()
	{
		// 宴表示中はリターン
		if (utageDisplayFlg == true) {
			return;
		}

		Vector3 tapPoint;
		if (PlayerId == PhotonNetwork.player.ID) {	// 今は、クリック者がIDなので一致していると操作者　　保存しているプレイヤーIDと自分のプレイヤーIDが違う?(つまり相手がパネルを生成して自分のターンになっている）
			tapPoint = Input.mousePosition;
		
			if (holdObj != null) {
				// holdOjbがFFTpanelの場合はリターン つまり動かさない
				if ("FFTpanel".Equals (holdObj.tag)) {
					return;
				}

				// もしドラッグ中に別の場所をタッチしたときにパネルが飛ばないように補正処理(WorldPointで計算）
				Vector3 pos1 = this.holdObj.transform.position;												// ドラッグ中のオブジェクトの位置
				Vector3 pos2 = Camera.main.ScreenToWorldPoint (new Vector3 (tapPoint.x, tapPoint.y, z));	// タップした位置
				pos2.z = pos1.z;											// よくわからんがZがゼロになったり10になったりするのでPos1とPos２を揃える		
				float dst = Vector3.Distance (pos1, pos2);					// 2点間距離（Zはぜろなので無視される）
				if (dst >= 2.0) {
					return;
				}										// 2.0の距離以上だった場合は、タップ位置が飛んだと判断してリターン

				// ドラッグ先の場所へホールドオブジェクトを移動
				this.holdObj.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (tapPoint.x, tapPoint.y, z));
				// ポジション補正処理（範囲を超えないようにする処理）
				tapPoint = proc_PositionHosei (this.holdObj.transform.position);	// 補正があった場合tapPoint(マウス)の位置も補正する(補正あってもなくてもセットしなおすが。。）

				Collider2D[] colSet = Physics2D.OverlapPointAll (Camera.main.ScreenToWorldPoint (new Vector3 (tapPoint.x, tapPoint.y, z)));
				if (colSet.Length > 1) {
					foreach (Collider2D col in colSet) {
						if (!col.gameObject.Equals (this.holdObj)) {		
							if (col.gameObject.tag == this.holdObj.tag) {	// 二つのオブジェクトのタグが同じ場合は移動可能
								float tmpPositionX = holdPositionX;
								float tmpPositionY = holdPositionY;
								holdPositionX = col.gameObject.transform.position.x;
								holdPositionY = col.gameObject.transform.position.y;
								col.gameObject.transform.position = new Vector3 (tmpPositionX, tmpPositionY, z);	// ホールドオブジェクトのもとにあった位置へ衝突オブジェクトを移動
								SpriteRenderer nowRenderer = col.gameObject.GetComponent<SpriteRenderer> ();	// 衝突されたほうのスプライトレンダラー
								changeSprite = nowRenderer.sprite;	// 入れ替わったスプライト保持
								shiftFlg = true;					// 入れ替わりが一回あったと判断してTrue パズドラでいう玉をドラッグしてタイマースタート

								// 入れ替わり時のサウンド再生処理
								// オーディオソース設定
								audioSource = gameObject.GetComponent<AudioSource> ();
								audioSource.clip = audioClip5;	// 移動音
								audioSource.Play ();

								// パネル移動回数を一回減らす
								if (MoveTimesPanel != 0) {
									MoveTimesPanel--;					// デクリメント
									MoveTimesText.GetComponent<TextMesh> ().text = " " + (MoveTimesPanel).ToString ();		// 最大移動回数

									// パネル移動回数がゼロになったらドラッグ終了したと同じ状態にする。
									if (MoveTimesPanel == 0) {
										LeftUpEvent ();		// ドラッグから離した時のイベントをここでも呼び出す
										return;				// 魔法陣やパーティクルを表示させないようにここでリターン
									}
								}
								

							}
						}
					}
				}
			} else {			// ホールドオブジェクトがない場合
				// 何もしない
			}
		} else {				// 操作者ではない場合
			// 数字の表示に関する処理だけを行う。
			// パネル移動回数を一回減らす
			if (MoveTimesPanel != 0) {
				//MoveTimesPanel--;					// デクリメントは操作者でないので行わない。
				MoveTimesText.GetComponent<TextMesh> ().text = " " + (MoveTimesPanel).ToString ();		// 最大移動回数

				// パネル移動回数がゼロになったらドラッグ終了したと同じ状態にする。
				if (MoveTimesPanel == 0) {
					LeftUpEvent ();		// ドラッグから離した時のイベントをここでも呼び出す
					return;				// 魔法陣やパーティクルを表示させないようにここでリターン
				}
			}
		}

		// ドラッグ開始時に実行
		// ２者とも共通で実行
		if (leftDrugFlg == false) {
			leftDrugFlg = true;
			// 同一呪文パネルを明るくする。
			blightSameSpell (holdObj, true);

			// 魔法陣を表示させる
			appareSpellCircle (true);

			// パーティクル生成 (今はドラッグ時は生成しない）　パネルが消える時に生成
			//setParticles (true);
		}
	}

	// ポジション補正処理
	private Vector3 proc_PositionHosei (Vector3 wkPos)
	{
		//移動先座標
		Vector3 position = wkPos;	// 結局ここもholdObjだが。。。

		//画面左下、右上のワールド座標をビューポートから取得
		//Vector3 min = Camera.main.ViewportToWorldPoint(new Vector3(0, 0,0));
		//Vector3 max = Camera.main.ViewportToWorldPoint(new Vector3(1, 1,0));
		Vector3 min = new Vector3 (-2, -2, 0);
		Vector3 max = new Vector3 (2, 2, 0);

		//移動先を補正する　Clampとは与えられた範囲内に補正する関数
		position.x = Mathf.Clamp (position.x, min.x, max.x);
		position.y = Mathf.Clamp (position.y, min.y, max.y);

		//補正済みの座標を反映
		this.holdObj.transform.position = position;

		return RectTransformUtility.WorldToScreenPoint (Camera.main, position);
	}

	// パーティクル生成
	private void setParticles (bool flg, int type)
	{
		// エフェクトランダム
		//int rnd = Random.Range (0, 3);					// これで０から２となるらしい。

		if (flg == true) {
			if (type == 2) {
				fireParticle.GetComponent<ParticleSystem> ().Play ();	// ファイヤー
			} else if (type == 1) {
				thunderEx.GetComponent<ParticleSystem> ().Play ();		// サンダー
			} else if (type == 0) {
				Transform[] tfs = stormEx.GetComponent<ParticleSystem> ().GetComponentsInChildren<Transform> ();
				foreach (Transform tf in tfs) {
					GameObject go = tf.gameObject;
					go.GetComponent<ParticleSystem> ().playbackSpeed = 4.0f;		// スピードを4倍にする　子のパーティクルを全て増加しないといけない！
				}
				stormEx.GetComponent<ParticleSystem> ().Play ();		// ストーム
			} else {
				fireParticle.GetComponent<ParticleSystem> ().Play ();
			}

			// タイマーは3秒をセット
			timeLeftFireEx = 3.0f;	// ３エフェクト共通
		} else {
			fireParticle.GetComponent<ParticleSystem> ().Stop ();
			thunderEx.GetComponent<ParticleSystem> ().Stop ();
			stormEx.GetComponent<ParticleSystem> ().Stop ();
		}
	}


	// 同一呪文パネルを明るくする。
	private void blightSameSpell (GameObject gm, bool blightFlg)
	{
		// ドラッグしているパネルを明るくする
		//Color	color = Color.white;		// ドラッグ中の呪文
		Color[] color_etc = new Color[TileLineNum];	// 呪文1-3の色
		color_etc [0] = Color.yellow;		// 呪文１ 	黄色
		color_etc [1] = Color.yellow;		// 呪文２		黄色
		color_etc [2] = Color.yellow;		// 呪文３		黄色
		if (blightFlg == true) {
			//color *= 1.5f;
			color_etc [0] *= 1.15f;
			color_etc [1] *= 1.15f;
			color_etc [2] *= 1.15f;
		} else {
			//戻す場合は白に変える（ドラッグ以外の呪文）
			color_etc [0] = Color.white;		// 呪文１
			color_etc [1] = Color.white;		// 呪文２
			color_etc [2] = Color.white;		// 呪文３
		}	
		//holdObj.GetComponent<Renderer>().material.color = color;

		// 同一呪文をチェック 
		int sameSpellNum = 0;
		for (int i = 0; i < TileLineNum; i++) {			//行
			for (int j = 0; j < TileColNum; j++) {		//列
				try {
					if (gm != null) {					// 何か要素があれば。
						//GameObject nextObj = this.tileSet[i,j+1];
						SpriteRenderer nowRenderer = gm.GetComponent<SpriteRenderer> ();
						//SpriteRenderer nextRenderer = nextObj.GetComponent<SpriteRenderer>();
						Sprite nowSprite = nowRenderer.sprite;
						//Sprite nextSprite = nextRenderer.sprite;
						if (nowSprite.Equals (correctTilesVol [i * TileColNum + j]) ||
						    nowSprite.Equals (clearTilesVol1 [i * TileColNum + j]) ||
						    nowSprite.Equals (clearTilesVol2 [i * TileColNum + j])) {	// 通常のタイル、行クリアタイル２種類がセットされていたら。

							// 同一呪文の行数を記憶
							sameSpellNum = i + 1;	// 1,2,3のどれか取得
						}
					}
				} catch {
				}
			}
		}

		// 同一呪文を明るくする
		if (sameSpellNum != 0) {								// ０以外ということは同一呪文があったということ。（必ずあるはずだが）
			//Sprite[] wkTiles = new Sprite[TileColNum];		//ドラッグ中のタイルの呪文
			Sprite[,] wkTiles_etc = new Sprite[TileLineNum, TileColNum];	//呪文１-３
			for (int i = 0; i < TileLineNum; i++) {			//行
				for (int zz = 0; zz < TileColNum; zz++) {	//列 ドラッグ中のタイル
					if ((sameSpellNum - 1) == i) {
						//wkTiles [zz] = correctTilesVol [(sameSpellNum - 1) * TileColNum + zz];	// ドラッグ中の呪文はここへ保存
					}
					wkTiles_etc [i, zz] = correctTilesVol [i * TileColNum + zz];	// 残りの呪文はここへ保存（ドラッグ中のも含む）
				}
			}
			for (int i = 0; i < TileLineNum; i++) {			//行
				for (int j = 0; j < TileColNum; j++) {		//列
					GameObject chkObj = tileSet_R [i, j];
					SpriteRenderer nowRenderer = chkObj.GetComponent<SpriteRenderer> ();
					//SpriteRenderer nextRenderer = nextObj.GetComponent<SpriteRenderer>();
					Sprite nowSprite = nowRenderer.sprite;

					// ドラッグ中以外の呪文(ドラッグ中の呪文も一旦色つける）
					for (int ii = 0; ii < TileLineNum; ii++) {			//行
						for (int zz = 0; zz < TileColNum; zz++) {		//列
							if (nowSprite.Equals (wkTiles_etc [ii, zz])) {		// 現在チェック中のタイルと一致したらその色を変える
								tileSet_R [i, j].GetComponent<Renderer> ().material.color = color_etc [ii];		// 明るく光らせる
								if (blightFlg == false) {
									//タイルのフェード設定
									//tileSet_R [i, j].GetComponent<Fadeout> ().setStartFlg(false);		// スプライトのフェードアウトをオフ	

									// タイルのテキスト表示を消す
									GameObject canvasGmObj;	// ワーク用キャンバス
									canvasGmObj = tileSet_R [i, j].transform.FindChild ("CntDownCvs").gameObject;
									if (canvasGmObj != null) {
										Text txt = canvasGmObj.GetComponentInChildren<Text> ();
										txt.text = "";	// 無表示
									}
								} else {
									//タイルのフェード設定
									//tileSet_R [i, j].GetComponent<Fadeout> ().setStartFlg(true);		// スプライトのフェードアウトをオン
									tileSet_R [i, j].GetComponent<Fadeout> ().setDelayTime ((float)zz / 2.0f);	// ディレイタイムを設定（zzをそのまま時間としてセット　1枚目なら０秒）

									// タイルのテキスト表示を変える
									GameObject canvasGmObj;	// ワーク用キャンバス
									canvasGmObj = tileSet_R [i, j].transform.FindChild ("CntDownCvs").gameObject;
									if (canvasGmObj != null) {
										Text txt = canvasGmObj.GetComponentInChildren<Text> ();
										txt.text = (zz + 1).ToString ();	// 1〜３を表示
									}
								}
								// ドラッグ中の呪文の場合はパーティクルも生成
								if ((sameSpellNum - 1) == ii) {		// ドラッグ中の呪文の場合
									GameObject dummyParticleObj = tileSet_R [i, j].transform.FindChild ("particleDrag").gameObject;	// パーティクルオブジェクト取得
									ParticleSystem ps = dummyParticleObj.GetComponent<ParticleSystem> ();
									if (blightFlg == true) {
										ps.gameObject.SetActive (true);
									} else {
										ps.gameObject.SetActive (false);
									}
								}
							}
						}
					}

					// ドラッグ中の呪文
					//for (int zz = 0; zz < TileColNum; zz++) {		//列
					//if (nowSprite.Equals (wkTiles[zz])) {		// 現在チェック中のタイルと一致したらその色を変える
					//	tileSet_R[i,j].GetComponent<Renderer>().material.color = color;		// 明るく光らせる
					//}
					//}

				}
			}
		}
	}

	private void appareSpellCircle (bool flg)
	{
		Vector3 temp = dataRune.transform.position;
		if (flg == true) {
			temp.z = -9.0f;		// Z軸を手前にすることで魔法陣表示
		} else {
			temp.z = -10.0f;	// Z軸を裏にして魔法陣隠す
		}

		dataRune.transform.position = temp;
	}

	// ドラッグ終了
	private void LeftUp ()
	{

		LeftUpEvent ();

	}

	// ドラッグから離した時の処理（パネル移動回数がゼロになったときも使用）
	private void LeftUpEvent ()
	{
		string enemyAtkText = "Minus 1";

		// 宴表示中はリターン
		if (utageDisplayFlg == true) {
			return;
		}

		if (holdObj != null) {
			if (shiftFlg == true) {		// オブジェクト移動があった？(Yes)
				shiftNum++;				// 移動回数カウントアップ


				if ((shiftMax == shiftNum) && (MoveTimesPanel == 0)) {
					// 敵攻撃ターン＆パネルゼロの場合
					//もし敵攻撃ターンになったら攻撃
					proc_PanelReset ();				// パネルリセット
					getDamageToFromEnemy (false, 0, 2);	// 敵攻撃(ダブル攻撃）
					shiftNum = 1;					// 攻撃回数をリセット
					// キャラ振動させる
					setCharaVibFlg (false);			// 自分が攻撃を受ける
					// キャラのスプライトをダメージ受けるスプライトへ（falseは自分キャラがダメージ）
					setCharaSprite (false);
					// 敵攻撃表示にかえる
					enemyAtkText = "2HIT!";	// 敵攻撃表示にかえる
				} else if (shiftMax == shiftNum) {
					//もし敵攻撃ターンになったら攻撃
					proc_PanelReset ();				// パネルリセット
					getDamageToFromEnemy (false, 0, 0);	// 敵攻撃(一定攻撃）
					shiftNum = 1;					// 攻撃回数をリセット
					// キャラ振動させる
					setCharaVibFlg (false);			// 自分が攻撃を受ける
					// キャラのスプライトをダメージ受けるスプライトへ（falseは自分キャラがダメージ）
					setCharaSprite (false);
					// 敵攻撃表示にかえる
					enemyAtkText = "Attack!";		// 敵攻撃表示にかえる
				} else if (MoveTimesPanel == 0) {
					// もしパネル移動回数になったらパネルリセット＆攻撃
					proc_PanelReset ();				// パネルリセット
					// パネル移動回数を初期化
					MoveTimesPanel_Hoji = MoveTimesPanel;							// ダメージ表示用に保持
					MoveTimesPanel = MovePanelMax;
					MoveTimesText.GetComponent<TextMesh> ().fontSize = 300;			// 一旦300へ（あとで500へ戻す）
					MoveTimesText.GetComponent<TextMesh> ().text = " Reset!!";		// Reset!!を表示
					timeMovePanel = 1.0f;											// 1秒表示維持

					// 敵のパネルゼロでの攻撃
					getDamageToFromEnemy (false, 0, 1);	// 敵攻撃(パネルゼロ攻撃）
					// キャラ振動させる
					setCharaVibFlg (false);			// 自分が攻撃を受ける
					// キャラのスプライトをダメージ受けるスプライトへ（falseは自分キャラがダメージ）
					setCharaSprite (false);
					// 敵攻撃表示にかえる
					enemyAtkText = "Attack!";		// 敵攻撃表示にかえる
				}

				// 敵攻撃回数表示
				//ShiftCntText.GetComponent<Text> ().text = " " + (shiftMax - shiftNum).ToString () + "回";		// 残り回数
				AtkTimesText.GetComponent<TextMesh> ().fontSize = 300;				// 一旦300へ（あとで500へ戻す）
				AtkTimesText.GetComponent<TextMesh> ().text = enemyAtkText;			// Minus 1 表示　または　Attack! 表示
				timeEnemyAtkCnt = 1.0f;												// 1秒表示維持
			}

			// ポジション移動
			if (PlayerId == PhotonNetwork.player.ID) {	// クリック者がIDなので一致していると操作者なのでポジションチェンジ
				holdObj.transform.position = new Vector3 (holdPositionX, holdPositionY, z);
			} else {
				// 操作されている側は、PhotonViewでオブジェクト制御されているので何もしない
			}

			// ドラッグ終了時に実行
			SetEffectLeaveDrag ();		// パネルのエフェクト系削除処理（ドラッグから手を離したときに実行）

			// パネル初期化処理（移動しようがしまいが）（ドラッグから手を離したときに実行）
			SetPanelInitLeaveDrag ();

		} else {						// holdObjがない場合はエフェクトだけ消す
			// ドラッグ終了時に実行
			SetEffectLeaveDrag ();		// パネルのエフェクト系削除処理（ドラッグから手を離したときに実行）
		}
	}

	// パネルのエフェクト系削除処理（ドラッグから手を離したときに実行）
	private void SetEffectLeaveDrag ()
	{
		if (leftDrugFlg == true) {
			leftDrugFlg = false;

			// ドラッグしているパネルの明るさを元に戻す
			blightSameSpell (holdObj, false);

			// 魔法陣を消す
			appareSpellCircle (false);

			// パーティクル消去
			setParticles (false, 0);
		}
	}

	// パネル初期化処理（移動しようがしまいが）（ドラッグから手を離したときとクリック開始時に実行）
	private void SetPanelInitLeaveDrag ()
	{
		holdObj = null;
		shiftFlg = false;				// シフトフラグを初期化
		// パネル移動した場合のみReset!!表示
		if (MoveTimesPanel != MovePanelMax) {								// Maxなら一度もパネル移動していないと判断
			MoveTimesText.GetComponent<TextMesh> ().fontSize = 300;			// 一旦300へ（あとで500へ戻す）
			MoveTimesText.GetComponent<TextMesh> ().text = " Reset!!";		// Reset!!を表示
			timeMovePanel = 1.0f;											// 1秒表示維持
			MoveTimesPanel_Hoji = MoveTimesPanel;							// ダメージ表示用に保存
			MoveTimesPanel = MovePanelMax;									// 最大値を再セット
		} else {																// 保険的にセット
			MoveTimesPanel_Hoji = MoveTimesPanel;							// ダメージ表示用に保存
			MoveTimesPanel = MovePanelMax;									// 最大値を再セット
		}
	}


	// 左側に初めにセットするボリュームスプライト
	private void SetTileSet_LStart ()
	{
//		GameObject[,] tileSet = new GameObject[TileLineNum,TileColNum];
//		for(int i = 0; i < TileLineNum; i++){
//			for(int j = 0; j < TileColNum; j++){
//				Collider2D col = Physics2D.OverlapPoint(new Vector2(FirstTilePosX + TileWidth * j, FirstTilePosY + TileHeight * i));
//				if("FFTpanel".Equals(col.tag)){
//					tileSet[i,j] = col.gameObject;
//				}
//			}
//		}
//		this.tileSet_L = tileSet;
	}

	// 右側に初めにセットするボリュームスプライト
	private void SetTileSet_RStart ()
	{
		GameObject[,] tileSet = new GameObject[TileLineNum, TileColNum];
		for (int i = 0; i < TileLineNum; i++) {
			for (int j = 0; j < TileColNum; j++) {
				Collider2D col = Physics2D.OverlapPoint (new Vector2 (FirstTilePosX_R + TileWidth * j, FirstTilePosY_R + TileHeight * i));
				if ("Volpanel".Equals (col.tag)) {
					tileSet [i, j] = col.gameObject;
				}
			}
		}
		this.tileSet_R = tileSet;

		// 同一呪文パネルをオフにする。※これは、パネルから発光するパーティクルをPlay on Awakeで設定しているため、それを一旦消すために呼び出している
		particleOffAllPanell (false);

	}

	// すべてのパネルのパーティクルをオフにする
	private void particleOffAllPanell (bool flg)
	{
		for (int ii = 0; ii < TileLineNum; ii++) {			//行
			for (int zz = 0; zz < TileColNum; zz++) {		//列
				GameObject dummyParticleObj = tileSet_R [ii, zz].transform.FindChild ("particleDrag").gameObject;	// パーティクルオブジェクト取得
				ParticleSystem ps = dummyParticleObj.GetComponent<ParticleSystem> ();
				if (flg == true) {
					ps.gameObject.SetActive (true);
				} else {
					ps.gameObject.SetActive (false);
				}
			}
		}
	}

	// 左側にあるオブジェクトを再セットするチェック用関数
	private void SetTileSetLeft (int clearNum, bool rstFlg)
	{
		GameObject[,] tileSet = new GameObject[TileLineNum, TileColNum];
		for (int i = 0; i < TileLineNum; i++) {
			for (int j = 0; j < TileColNum; j++) {
				Collider2D col = Physics2D.OverlapPoint (new Vector2 (FirstTilePosX + TileWidth * j, FirstTilePosY + TileHeight * i));
				if ("FFTpanel".Equals (col.tag)) {
					tileSet [i, j] = col.gameObject;

					// スプライトを変更する処理が以下
					if (clearNum != 0) {
						int thisRow = clearNum - 1;		// 今回該当する行数
						SpriteRenderer nowRenderer = col.gameObject.GetComponent<SpriteRenderer> ();
						if ((rstFlg == true) && (thisRow == i)) {		// 成功フラグがtrueでかつ行数が同じ場合
							// nowRenderer.sprite = clearTilesFFT1 [i * TileColNum + j];	// 今はクリアスプライトを入れない。
						} else if ((rstFlg == false) && (thisRow == i)) {		// 成功フラグがfalseでかつ行数が同じ場合
							//for(int i = 0; i < TileLineNum; i++){			//１行だけでいい
							for (int tileNum = 0; tileNum < TileLineNum * TileColNum; tileNum++) {	// 同じスプライト画像を検索する
								if ((nowRenderer.sprite == clearTilesFFT1 [tileNum]) ||
								    (nowRenderer.sprite == clearTilesFFT2 [tileNum])) {				// 成功１か２か
									nowRenderer.sprite = correctTilesFFT [tileNum];	// 元のスプライト画像へ戻す処理
								}
							}
						}
					}
				}
			}
		}
		this.tileSet_L = tileSet;
	}

	// 右側にあるオブジェクトを再セットするチェック用関数
	private void SetTileSetRight (int clearNum, bool rstFlg, int clearCorrectRowNum)
	{	// clearNum = クリアする行　clearCorrectRowNum = どの行数でクリアしたか。
		GameObject[,] tileSet = new GameObject[TileLineNum, TileColNum];
		for (int i = 0; i < TileLineNum; i++) {
			for (int j = 0; j < TileColNum; j++) {
				Collider2D col = Physics2D.OverlapPoint (new Vector2 (FirstTilePosX_R + TileWidth * j, FirstTilePosY_R + TileHeight * i));
				if (col != null) {		// 衝突物あり?(Yes)
					if ("Volpanel".Equals (col.tag)) {
						tileSet [i, j] = col.gameObject;

						// スプライトを変更する処理が以下
						if (clearNum != 0) {
							int thisRow = clearNum - 1;		// 今回該当する行数
							SpriteRenderer nowRenderer = col.gameObject.GetComponent<SpriteRenderer> ();
							if ((rstFlg == true) && (thisRow == i)) {		// 成功フラグがtrueでかつ行数が同じ場合
								//nowRenderer.sprite = clearTilesVol2 [i * TileColNum + j];		// 今は完全にクリアーを表示
								nowRenderer.sprite = clearTilesVol2 [clearCorrectRowNum * TileColNum + j];		// 今は完全にクリアーを表示(クリアした行数でスプライトを置換）
							} else if ((rstFlg == false) && (thisRow == i)) {		// 成功フラグがfalseでかつ行数が同じ場合
								//for(int i = 0; i < TileLineNum; i++){			//１行だけでいい
								for (int tileNum = 0; tileNum < TileLineNum * TileColNum; tileNum++) {	// 同じスプライト画像を検索する
									if ((nowRenderer.sprite == clearTilesVol1 [tileNum]) ||
									    (nowRenderer.sprite == clearTilesVol2 [tileNum])) {
										nowRenderer.sprite = correctTilesVol [tileNum];	// 元のスプライト画像へ戻す処理
									}
								}
							}
						}
					}
				}
			}
		}
		this.tileSet_R = tileSet;
	}


	// 一致したタイルを消す処理
	private void DeleteMatchTile ()
	{

		// 宴表示中はリターン
		if (utageDisplayFlg == true) {
			return;
		}

		int goalFlgNum = 0;		// マス数分カウントしてあればゴール！
		int rowClearNum = 0;	// 各行の一致確認カウンタ
		bool panelVanishFlg = false;	// パネルをリセットするかのローカルフラグ

		// 現在の左のタイルセット情報を得る
		//SetTileSetLeft(0,false);		// 0はただ情報を得るだけ
		// 現在の右のタイルセット情報を得る
		SetTileSetRight (0, false, 0);		// 0はただ情報を得るだけ(第三引数の０は仮。関数内で使用しない）

		// 右のタイルをチェック
		for (int i = 0; i < TileLineNum; i++) {					// 行

			int hitRowNum = -1;		// nowSpriteが一致したのはcorrectTilesVolの何行目のタイルかを記憶

			for (int j = 0; j < TileColNum; j++) {				// 列
				try {
					// タイルの一致チェック
					GameObject nowObj = this.tileSet_R [i, j];
					if (nowObj != null) {					// 何か要素があれば。
						//GameObject nextObj = this.tileSet[i,j+1];
						SpriteRenderer nowRenderer = nowObj.GetComponent<SpriteRenderer> ();
						//SpriteRenderer nextRenderer = nextObj.GetComponent<SpriteRenderer>();
						Sprite nowSprite = nowRenderer.sprite;
						//Sprite nextSprite = nextRenderer.sprite;

						// どの行でも揃っていれば成功とするため、iiで回す
						for (int ii = 0; ii < TileLineNum; ii++) {
							if (nowSprite.Equals (correctTilesVol [ii * TileColNum + j]) ||
							    nowSprite.Equals (clearTilesVol1 [ii * TileColNum + j]) ||
							    nowSprite.Equals (clearTilesVol2 [ii * TileColNum + j])) {	// 通常のタイル、行クリアタイル２種類がセットされていたら。
								if (j == 0) {				// １列目
									hitRowNum = ii;		// 何行目のタイルかを記憶
									goalFlgNum++;		// 最後まで+1が入り続ければクリア
									rowClearNum++;		// 行クリアカウント
								} else {					// 2列目以降
									if (ii == hitRowNum) {	// １列目でヒットした行数とiiが同じ行数であればそれは一致した呪文と判断
										goalFlgNum++;		// 最後まで+1が入り続ければクリア
										rowClearNum++;		// 行クリアカウント
									}
								}

								// クリアカウントがタイルの列数と一致したら、それは1行分すべて一致したと判断（＝呪文完成）
								if (rowClearNum == TileColNum) {
									if (clearList [i, 1] == false) {
										clearList [i, 1] = true;		// クリアフラグセット
									}
									// 揃ったときの処理⇩
									// ****************ここで今までCLEAR表示に入れ替えていたが、入れ替えないようにした！**************
									//SetTileSetRight (i + 1, true, hitRowNum);	// i行をクリアへ（一旦1をプラスしている）
									// *************************************************************************************
									hitCntOneAtk++;					// ヒット回数を１つカウントアップ（最大３回）
									// ここでダメージを１回与える！
									getDamageToFromEnemy (true, 0, 0);
									// パネルをリセットすることが確定
									panelVanishFlg = true;
									// 炎のエフェクト生成
									setParticles (true, ii);	// ii=0 緑　ii=1 黄　ii=2 赤
									// キャラ振動させる
									setCharaVibFlg (true);
									// キャラのスプライトをダメージ受けるスプライトへ（Trueは敵キャラがダメージ）
									setCharaSprite (true);

									rowClearNum = 0;				// 行クリアしたので一旦0クリア
								}
								// クリアの条件に引っかかったらそこでiiのループは抜ける
								break;
							} else {
								if (j == 0) {		// 列が１列目の場合は必ずリセット
									rowClearNum = 0;			// 行カウンタリセット
									clearList [i, 1] = false;		// クリアフラグリセット
									SetTileSetRight (i + 1, false, 0);	// 行は一致していないので、スプライトを元に戻す処理(第三引数の０は仮。関数内で使用はしない）
									//break;						// 一つでも不一致があれば次の行をチェック → iiでループするためブレークしない
								} else if ((j >= 1) && (hitRowNum == ii)) {// ２列目以上でかつ、1列目のヒット行がiiだった場合にここに入ってきた場合はクリアフラグリセット
									// つまり、１行目はヒットしているが、２行目が異なる場合。さらに緑のパズル(i=0)のとき。
									rowClearNum = 0;			// 行カウンタリセット
									clearList [i, 1] = false;		// クリアフラグリセット
									SetTileSetRight (i + 1, false, 0);	// 行は一致していないので、スプライトを元に戻す処理(第三引数の０は仮。関数内で使用はしない）
								}
							}
						}
						
					}
				} catch {
				}
			}

		}

		// 左と右の行が一致している場合のチェック
		//checkTileRow();

		// クリアチェック
		if (goalFlgNum == (TileLineNum * TileColNum) * 2) {		// マス数分*2あればクリア(3*3マスが２つの場合、9*2=18ならクリア)
			if (clearOnceFlg == false) {
				GameObject charaObj = charaImage;
				SpriteRenderer charaRenderer = charaObj.GetComponent<SpriteRenderer> ();
				charaRenderer.sprite = charaList [1];						// クリアキャラ
				ClearText.GetComponent<Text> ().text = "くりあ〜！！おめでとう！！";		// くりあ〜テキスト出現
				// なっている音があれば消す
				proc_StopAudio ();
				// 祝福ボイス
				audioSource.clip = audioClip4;		// 原ボイス
				audioSource.Play ();
				// クリアフラグをセット（二度とここは通らない）
				clearOnceFlg = true;
				// タイルにクリアー表示
				SetTileSetLeft (4, true);		// 4はクリア番号
			}
		}


		// パネルリセットフラグがオンならリセット
		if (panelVanishFlg == true) {
			proc_PanelReset ();
//			MoveTimesPanel_Hoji = MoveTimesPanel;	// ダメージ表示用に保持
//			MoveTimesPanel = MovePanelMax;	// パネルの移動回数をMax表示にリセット
//			MoveTimesText.GetComponent<TextMesh> ().text = " " + (MoveTimesPanel).ToString ();		// 最大移動回数
		}

		// ヒット回数＠１回の攻撃をクリア
		hitCntOneAtk = 0;
			
	}

	// キャラ振動フラグをセットする処理
	private void setCharaVibFlg (bool charaFlg)
	{	// 引数 true：敵が振動　false：自分が振動
		if (charaFlg == true) {
			EnemyHitFlg = true;		// フラグを立てるとアニメーターの繊維フラグがTrueのルートへ行き、バイブする
		} else {
			MeCharaHitFlg = true;	// フラグを立てるとアニメーターの繊維フラグがTrueのルートへ行き、バイブする
		}
	}

	// キャラのダメージスプライトをセットする処理
	private void setCharaSprite (bool charaFlg)
	{	// 引数 true：敵がダメージ　false：自分がダメージ
		if (charaFlg == true) {
			// スプライトをダメージ顔に。敵キャラ
			EnemyChara ec = enemyChara.GetComponent<EnemyChara> ();
			ec.proc_ChangeEnemySpriteToDamege ();
		} else {
			// スプライトをダメージ顔に。自分キャラ
			MeChara mc = meChara.GetComponent<MeChara> ();
			mc.proc_ChangeMeCharaSpriteToDamege (enemyAtkType);
		}
	}

	// パネルリセット処理
	private void proc_PanelReset ()
	{
		int i;
		panelInitFlg = true;			// フラグセット
		randomList_Vol.Clear ();		// ランダムリストをクリア
//		for (i = 0; i < PanelRoot.GetComponent<PhotonDataTrans> ().randomList_Vol_Photon.Length; i++) {
//			PanelRoot.GetComponent<PhotonDataTrans> ().randomList_Vol_Photon [i] = 9;	// Photonのランダム配列もクリア 9はありえない数字なので初期値でこれをセット
//		}
		//PanelRoot.GetComponent<PhotonDataTrans> ().panelInitFlgPhoton = 0;				// パネル初期化を未完了へ

		// テスト　Photon 
		// PanelRoot.GetComponent<PhotonDataTrans> ().hensu1 = 5;


		// 炎のエフェクト削除
		//setParticles(false);
	}

	// 敵へダメージを与える処理
	private void getDamageToFromEnemy (bool ToFromFlg, int DamageTypeMe, int DamageTypeEnemy)
	{	// True:こちらから攻撃　False:敵から攻撃受ける
		// DamageTypeMe		0:未使用
		// DamageTypeEnemy	0:敵の一定攻撃		1:パネル移動回数０のときの敵の攻撃
		// ゲージのスケールが0.5fなので　0.5fがフルのゲージ状態

		// 宴フラグがオンなら処理しない
		if (utageDisplayFlg == true) {
			return;
		}

		// 敵の最大体力は10000(0.5f)  1ダメージは100(0.005f)  
		float oneDamage = 0.0f;								// 一回のダメージ量

		GameObject gauge;
		Vector3 v3;

		if (ToFromFlg == true) {
			gauge = PGauge2;								// 敵ゲージ
			oneDamage = oneDamageEnemy * MoveTimesPanel_Hoji;		// パネル移動回数が多いほど大ダメージ（つまり移動回数が少なく揃えた場合）
			meAtkType = 0;										// 自キャラアタックタイプゼロ（パネル揃えた
		} else {
			gauge = PGauge1;								// こちらのゲージ
			if (DamageTypeEnemy == 0) {
				oneDamage = oneDamageMe1;						// 一回分のダメージ(敵からの攻撃は３倍分)
				enemyAtkType = 0;								// 敵キャラアタックタイプゼロ（定期攻撃）
			} else if (DamageTypeEnemy == 1) {
				oneDamage = oneDamageMe2;						// パネルゼロ攻撃
				enemyAtkType = 1;								// 敵キャラアタックタイプ１（パネルゼロ攻撃）
			} else if (DamageTypeEnemy == 2) {
				oneDamage = oneDamageMe1 + oneDamageMe2;		// ダブル攻撃
				enemyAtkType = 2;								// 敵キャラアタックタイプ２（ダブル攻撃）
			}
		}

		// ダメージを与えるもしくは食らう処理
		Vector3 v3now = new Vector3 (oneDamage, 0, 0);		// マイナスを加算
		v3 = gauge.transform.localScale + v3now;			// 今のスケール　＋　マイナス


		// ダメージ量によってエンディングかどうか判断
		if (v3.x > 0.0f) {									
			gauge.transform.localScale = v3;				// ダメージ与える	もしくは食らう
		} else {
			// エンディングフラグオン
			endingFlg = true;

			gauge.transform.localScale = new Vector3 (0, 1, 1);	// ライフ0に。
			//パネルもリセットしておく
			proc_PanelReset ();

			// なっている音があればけす
			proc_StopAudio ();

			// エンディング（敵からの攻撃を優先して処理する＝同時にライフがゼロになったら敵が勝利する！！）
			if (ToFromFlg == false) {						// 相手からの攻撃なので負けた
				//EndingB
				scenarioLabel = "EndingB";

				// 宴呼び出し
				StartCoroutine (CoTalk ());
				utageDisplayFlg = true;			// ここで宴プレイ中フラグオン
				// キャンバス一旦オフ（宴が消えたらオンにする）
				canvas.enabled = false;
			} else {										// こちらからの攻撃なので勝利した
				//EndingA
				scenarioLabel = "EndingA";

				// 宴呼び出し
				StartCoroutine (CoTalk ());
				utageDisplayFlg = true;			// ここで宴プレイ中フラグオン
				// キャンバス一旦オフ（宴が消えたらオンにする）
				canvas.enabled = false;
			}
		}
	}

	private void checkTileRow ()
	{
		GameObject[,] tileSetFFT = new GameObject[TileLineNum, TileColNum];
		GameObject[,] tileSetVol = new GameObject[TileLineNum, TileColNum];
		for (int i = 0; i < TileLineNum; i++) {
			// ここでFFTとVOLの各行が一致かどうか判定
			//if ((clearList [i, 0] == true) && (clearList [i, 1] == true)) {
			if (clearList [i, 1] == true) {					// 今は右の一致だけを見る。（左のパネルは画面外へ移動したので）
				if (rowClearList [i] == false) {
					if (i == 0) {
						audioSource.clip = audioClip3;		// 原田ボイス
					} else if (i == 1) {
						audioSource.clip = audioClip2;		// 加藤ボイス
					} else if (i == 2) {
						audioSource.clip = audioClip1;		// 阿澄ボイス
					}

					// 再生中ならば消す処理
					proc_StopAudio ();

					// ボイス再生
					audioSource.Play ();
				}
				// 行成功フラグセット
				rowClearList [i] = true;

				// クリアの表示
				for (int j = 0; j < TileColNum; j++) {
					Collider2D colFFT = Physics2D.OverlapPoint (new Vector2 (FirstTilePosX_R + TileWidth * j, FirstTilePosY_R + TileHeight * i));
					Collider2D colVol = Physics2D.OverlapPoint (new Vector2 (FirstTilePosX + TileWidth * j, FirstTilePosY + TileHeight * i));
					if (("FFTpanel".Equals (colFFT.tag)) && ("Volpanel".Equals (colVol.tag))) {
						tileSetFFT [i, j] = colFFT.gameObject;
						tileSetVol [i, j] = colVol.gameObject;

						SpriteRenderer nowRendererFFT = colFFT.gameObject.GetComponent<SpriteRenderer> ();
						SpriteRenderer nowRendererVol = colVol.gameObject.GetComponent<SpriteRenderer> ();

						nowRendererFFT.sprite = clearTilesFFT2 [i * TileColNum + j];
						nowRendererVol.sprite = clearTilesVol2 [i * TileColNum + j];
					}
				}
			}
		}
	}

	private void proc_StopAudio ()
	{
		// なっている音があれば消す（FFT)
		GameObject[] objList_FFT = GameObject.FindGameObjectsWithTag ("FFTpanel");
		foreach (GameObject gm in objList_FFT) {
			AudioSource panelAudio = gm.GetComponent<AudioSource> ();	// オーディオソースを取得
			if (panelAudio.isPlaying == true) {							// 再生中ならば
				panelAudio.Stop ();										// 停止
			}
		}
		// なっている音があれば消す(Volume)
		GameObject[] objList_Vol = GameObject.FindGameObjectsWithTag ("Volpanel");
		foreach (GameObject gm in objList_Vol) {
			AudioSource panelAudio = gm.GetComponent<AudioSource> ();	// オーディオソースを取得
			if (panelAudio.isPlaying == true) {							// 再生中ならば
				panelAudio.Stop ();										// 停止
			}
		}
	}

	// 宴フラグOFF
	public void setUtageFlgOff ()
	{
		utageDisplayFlg = false;

		// キャンバスをオンにする（宴が消えたからオンにする）
		canvas.enabled = true;

		// メインカメラのimage effectをオフ		別のネームスペースへアクセスしないといけない。
		mc.gameObject.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain> ().enabled = false;
		mc.gameObject.GetComponent<UnityStandardAssets.ImageEffects.Blur> ().enabled = false;


		if (endingFlg == true) {
			// シーンを再読み込み
			UnityEngine.SceneManagement.SceneManager.LoadScene ("title");
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotonDataTrans : Photon.MonoBehaviour
{
	public GameSystem gameSys;
	// GameSystemのオブジェクト

	//同期する変数1
	public int hensu1 = 0;
	public float hensu2 = 0f;
	public int[] randomList_Vol_Photon;
	// ランダムに選ばれたタイルNo(Vol) フォトン受け渡し用
	public int panelInitFlgPhoton = 0;
	// パネル初期化が完了しているかの判定フラグ（1:完了している)

	// Use this for initialization
	void Start ()
	{
		gameSys = GameObject.Find ("GameSystem").GetComponent<GameSystem> ();	// GameSystemオブジェクトをパブリック変数へセットする
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	// 変数のやり取りをする関数（streamがisWritingだと書き込み。それ以外は読み込み）
	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			//データの送信
			stream.SendNext (hensu1);
			stream.SendNext (hensu2);
			stream.SendNext (randomList_Vol_Photon [0]);
			stream.SendNext (randomList_Vol_Photon [1]);
			stream.SendNext (randomList_Vol_Photon [2]);
			stream.SendNext (randomList_Vol_Photon [3]);
			stream.SendNext (randomList_Vol_Photon [4]);
			stream.SendNext (randomList_Vol_Photon [5]);
			stream.SendNext (randomList_Vol_Photon [6]);
			stream.SendNext (randomList_Vol_Photon [7]);
			stream.SendNext (randomList_Vol_Photon [8]);
			stream.SendNext (panelInitFlgPhoton);
		} else {
			//データの受信
			this.hensu1 = (int)stream.ReceiveNext ();
			this.hensu2 = (float)stream.ReceiveNext ();
			this.randomList_Vol_Photon [0] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [1] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [2] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [3] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [4] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [5] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [6] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [7] = (int)stream.ReceiveNext ();
			this.randomList_Vol_Photon [8] = (int)stream.ReceiveNext ();
			this.panelInitFlgPhoton = (int)stream.ReceiveNext ();
		}
	}
}

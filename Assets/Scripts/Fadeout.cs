﻿using UnityEngine;
using System.Collections;

public class Fadeout : Photon.MonoBehaviour
{
	public float fadeTime = 0.5f;
	// フェードタイム
	public bool	startFlg = false;
	public float delayTimeDef;
	// 初期値のディレイタイム

	private float delayTime;
	private float currentRemainTime;
	private SpriteRenderer spRenderer;

	// Use this for initialization
	void Start ()
	{
		// 初期化
		currentRemainTime = fadeTime;
		spRenderer = GetComponent<SpriteRenderer> ();
	}

	// Update is called once per frame
	void Update ()
	{

		// フラグがオフの場合はアルファ値を戻してリターン
		if (startFlg == false) { 
			currentRemainTime = fadeTime;
			delayTime = delayTimeDef;					// ディレイタイムも初期化
			//return;
		} else {
			// まずはディレイタイムがゼロになるまで待つ
			delayTime -= Time.deltaTime;
			if (delayTime <= 0f) {
				// 残り時間を更新
				currentRemainTime -= Time.deltaTime;

				// 残り時間がゼロ
				if (currentRemainTime <= 0f) {
					// 残り時間が無くなったら自分自身を消滅
					//GameObject.Destroy(gameObject);
					//残り時間が無くなったら再び復帰させる。
					currentRemainTime = fadeTime;				// フェードタイムを再設定
					delayTime = fadeTime * 3;	// ディレイタイムのデフォルトとフェードタイム×3を設定（のこり２枚のフェードが終わってからこのタイルのフェードをさせるために×3している）
					//return;
				}
			}
		}

		// フェードアウト
		float alpha = currentRemainTime / fadeTime;
		var color = spRenderer.color;
		color.a = alpha;
		spRenderer.color = color;
	}

	// スタートフラグをセット
	public void setStartFlg (bool flg)
	{
		startFlg = flg;
	}
	// ディレイタイムを設定する
	public void setDelayTime (float time)
	{
		delayTimeDef = time;	// デフォルトを設定
		delayTime = time;		// ここですぐディレイタイムも設定
	}
}